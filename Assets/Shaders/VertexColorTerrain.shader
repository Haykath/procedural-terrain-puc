﻿Shader "Haykath/Terrain/VertexColorTerrain" {
	Properties {
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("MiddleTexture", 2D) = "white" {}
		_TopTex("TopTexture", 2D) = "white" {}
		_BotTex("BottomTexture", 2D) = "white" {}
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _TopTex;
		sampler2D _MainTex;
		sampler2D _BotTex;

		struct Input {
			float2 uv_MainTex;
			float2 uv_TopTex;
			float2 uv_BotTex;
			float4 color : COLOR;
		};

		fixed4 _Color;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 top = tex2D(_TopTex, IN.uv_TopTex);
			fixed4 mid = tex2D(_MainTex, IN.uv_MainTex);
			fixed4 bot = tex2D(_BotTex, IN.uv_BotTex);

			float4 c = saturate(bot*IN.color.b + mid * IN.color.g + top * IN.color.r);
			/*
			float4 c = float4(0,0,0,1);
			c = lerp(c, bot, IN.color.b);
			c = lerp(c, mid, IN.color.g);
			c = lerp(c, top, IN.color.r); */

			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
