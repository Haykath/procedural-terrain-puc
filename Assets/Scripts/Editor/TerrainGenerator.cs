﻿using System.Collections.Generic;

using UnityEditor;

using UnityEngine;

public class TerrainGenerator
{
    protected const float SmallThreshold = .005f;

    public TerrainGenerator(Texture2D heightMap, int widthVertexCount, int depthVertexCount, GenerationMode normalGeneration = GenerationMode.Smooth)
    {
        this.HeightMap = heightMap;
        this.SmoothingIterations = 1;
        this.NormalGeneration = normalGeneration;

        this.WidthVertexCount = widthVertexCount;
        this.DepthVertexCount = depthVertexCount;
        this.WidthScale = 1;
        this.DepthScale = 1;

        this.IsAbsoluteHeight = false;
        this.OffsetEdgeUvs = true;
        this.MinHeight = 0;
        this.MaxHeight = 1;

        this.BottomColor = Color.blue;
        this.MiddleColor = Color.green;
        this.TopColor = Color.red;
        this.BottomTextureThreshold = .33f;
        this.TopTextureThreshold = .67f;
        this.Blending = .01f;
    }

    public enum GenerationMode
    {
        Flat,
        Smooth
    }

    public Texture2D HeightMap { get; set; }

    public bool IsAbsoluteHeight { get; set; }

    public bool OffsetEdgeUvs { get; set; }

    public GenerationMode NormalGeneration { get; set; }

    // Width and depth

    public int WidthVertexCount { get; set; }

    public int DepthVertexCount { get; set; }

    public float WidthScale { get; set; }

    public float DepthScale { get; set; }

    // Height

    public float MaxHeight { get; set; }

    public float MinHeight { get; set; }

    public uint SmoothingIterations { get; set; }

    // Color

    public Color TopColor { get; set; }

    public Color MiddleColor { get; set; }

    public Color BottomColor { get; set; }

    public float TopTextureThreshold { get; set; }

    public float BottomTextureThreshold { get; set; }

    public float Blending { get; set; }

    // Mehtods
    public Mesh GenerateMesh()
    {
        Mesh generatedMesh = new Mesh();

        Debug.Log("Generating Terrain Mesh...");

        Debug.Log("Generating Vertices...");
        this.GenerateVertices(ref generatedMesh);
        Debug.Log("Applying vertex smoothing...");
        this.LaplacianSmooth(ref generatedMesh);
        Debug.Log("Calculating vertex colors...");
        this.CalculateColors(ref generatedMesh);
        if (this.NormalGeneration == GenerationMode.Flat)
        {
            Debug.Log("Duplicating vertices for flat normals...");
            this.MakeFlat(ref generatedMesh);
        }
        Debug.Log("Calculating normals...");
        this.CalculateNormals(ref generatedMesh);

        Debug.Log("All done!");
        return generatedMesh;
    }

    protected void GenerateVertices(ref Mesh mesh)
    {
        Vector3[] generatedVertexArray = new Vector3[this.WidthVertexCount * this.DepthVertexCount];
        Vector2[] generatedUvArray = new Vector2[this.WidthVertexCount * this.DepthVertexCount];
        List<int> indexlist = new List<int>();

        float widthStep = 1f / (float)(this.WidthVertexCount - 1);
        float depthStep = 1f / (float)(this.DepthVertexCount - 1);

        for (int d = 0; d < this.DepthVertexCount; d++)
        {
            for (int w = 0; w < this.WidthVertexCount; w++)
            {
                Vector2 uv = new Vector2(w * widthStep, d * depthStep);
                Vector3 vert = new Vector3(uv.x * this.WidthScale, 0, uv.y * this.DepthScale);

                if (this.OffsetEdgeUvs)
                {
                    uv.x -= uv.x >= 1 ? TerrainGenerator.SmallThreshold : 0;
                    uv.y -= uv.y >= 1 ? TerrainGenerator.SmallThreshold : 0;
                }

                float sampledHeight = 0;
                try
                {
                    sampledHeight = this.HeightMap.GetPixelBilinear(uv.x, uv.y).r;
                }
                catch (UnityException)
                {
                    this.SetTextureImporterFormat(this.HeightMap, true);
                    sampledHeight = this.HeightMap.GetPixelBilinear(uv.x, uv.y).r;
                }

                if (!this.IsAbsoluteHeight)
                {
                    sampledHeight = Mathf.Lerp(this.MinHeight, this.MaxHeight, sampledHeight);
                }

                vert.y = sampledHeight;

                int vertIndex = this.IndexFromWidthAndDepth(w, d);

                if (w < this.WidthVertexCount - 1 && d < this.WidthVertexCount - 1)
                {
                    int v1 = this.IndexFromWidthAndDepth(w, d + 1);
                    int v2 = this.IndexFromWidthAndDepth(w + 1, d + 1);

                    indexlist.Add(vertIndex);
                    indexlist.Add(v1);
                    indexlist.Add(v2);
                }

                if (w > 0 && d > 0)
                {
                    int v1 = this.IndexFromWidthAndDepth(w, d - 1);
                    int v2 = this.IndexFromWidthAndDepth(w - 1, d - 1);

                    indexlist.Add(vertIndex);
                    indexlist.Add(v1);
                    indexlist.Add(v2);
                }

                generatedVertexArray[vertIndex] = vert;
                generatedUvArray[vertIndex] = uv;

            }
        }

        // After everything is generated
        mesh.vertices = generatedVertexArray;
        mesh.uv = generatedUvArray;
        mesh.SetIndices(indexlist.ToArray(), MeshTopology.Triangles, 0);
    }

    protected Vector3 CalculateTriangleNormal(Mesh m, int v0, int v1, int v2)
    {
        Vector3 toV1 = m.vertices[v1] - m.vertices[v0];
        Vector3 toV2 = m.vertices[v2] - m.vertices[v0];

        return Vector3.Cross(toV1, toV2);
    }

    protected void CalculateNormals(ref Mesh mesh)
    {
        Vector3[] generatedNormals = new Vector3[mesh.vertices.Length];

        for (int i = 0; i < mesh.triangles.Length; i++)
        {
            int v0 = mesh.triangles[i];
            int v1 = mesh.triangles[++i];
            int v2 = mesh.triangles[++i];

            Vector3 normal = this.CalculateTriangleNormal(mesh, v0, v1, v2);

            generatedNormals[v0] += normal;
            generatedNormals[v1] += normal;
            generatedNormals[v2] += normal;
        }

        foreach (var normal in generatedNormals)
        {
            normal.Normalize();
        }

        mesh.normals = generatedNormals;
    }

    protected void CalculateColors(ref Mesh mesh)
    {
        Color[] generatedColorArray = new Color[mesh.vertices.Length];

        float absoluteBotThreshold = this.IsAbsoluteHeight ? this.BottomTextureThreshold : Mathf.Lerp(this.MinHeight, this.MaxHeight, this.BottomTextureThreshold);
        float absoluteTopThreshold = this.IsAbsoluteHeight ? this.TopTextureThreshold : Mathf.Lerp(this.MinHeight, this.MaxHeight, this.TopTextureThreshold);

        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            Color col = this.BottomColor;
            float vertexHeight = mesh.vertices[i].y;

            float t = Mathf.InverseLerp(absoluteBotThreshold - this.Blending, absoluteBotThreshold + this.Blending, vertexHeight);
            col = Color.Lerp(col, this.MiddleColor, t);

            t = Mathf.InverseLerp(absoluteTopThreshold - this.Blending, absoluteTopThreshold + this.Blending, vertexHeight);
            col = Color.Lerp(col, this.TopColor, t);

            generatedColorArray[i] = col;
        }

        mesh.colors = generatedColorArray;
    }

    protected void MakeFlat(ref Mesh mesh)
    {
        List<Vector3> generatedVertexList = new List<Vector3>();
        List<Vector2> generatedUvList = new List<Vector2>();
        List<Color> generatedColorList = new List<Color>();
        List<int> generatedIndexList = new List<int>();

        int indexAccum = 0;
        for (int t = 0; t < mesh.triangles.Length; t++)
        {
            int index = mesh.triangles[t];

            generatedVertexList.Add(mesh.vertices[index]);
            generatedUvList.Add(mesh.uv[index]);
            generatedColorList.Add(mesh.colors[index]);

            generatedIndexList.Add(indexAccum++);
        }

        mesh.vertices = generatedVertexList.ToArray();
        mesh.uv = generatedUvList.ToArray();
        mesh.colors = generatedColorList.ToArray();
        mesh.SetIndices(generatedIndexList.ToArray(), MeshTopology.Triangles, 0);
    }

    protected int IndexFromWidthAndDepth(int w, int d)
    {
        return (d * this.WidthVertexCount) + w;
    }

    protected void LaplacianSmooth(ref Mesh mesh)
    {
        if (this.SmoothingIterations <= 0)
        {
            return;
        }

        Vector3[] smoothedVertices = new Vector3[this.WidthVertexCount * this.DepthVertexCount];

        for (int i = 0; i < this.SmoothingIterations; i++)
        {
            for (int d = 0; d < this.DepthVertexCount; d++)
            {
                for (int w = 0; w < this.WidthVertexCount; w++)
                {
                    int vertIndex = this.IndexFromWidthAndDepth(w, d);

                    int accum = 0;
                    Vector3 newPos = Vector3.zero;

                    if (w < this.WidthVertexCount - 1)
                    {
                        newPos += mesh.vertices[this.IndexFromWidthAndDepth(w + 1, d)] - mesh.vertices[vertIndex];
                        accum++;
                    }
                    if (w > 0)
                    {
                        newPos += mesh.vertices[this.IndexFromWidthAndDepth(w - 1, d)] - mesh.vertices[vertIndex];
                        accum++;
                    }
                    if (d < this.DepthVertexCount - 1)
                    {
                        newPos += mesh.vertices[this.IndexFromWidthAndDepth(w, d + 1)] - mesh.vertices[vertIndex];
                        accum++;
                    }
                    if (d > 0)
                    {
                        newPos += mesh.vertices[this.IndexFromWidthAndDepth(w, d - 1)] - mesh.vertices[vertIndex];
                        accum++;
                    }

                    newPos /= (float)accum;
                    smoothedVertices[vertIndex] = mesh.vertices[vertIndex] + newPos;
                }
            }

            mesh.vertices = smoothedVertices;
        }
    }

    private void SetTextureImporterFormat(Texture2D texture, bool isReadable)
    {
        if (texture == null)
        {
            return;
        }

        string assetPath = AssetDatabase.GetAssetPath(texture);
        var texImporter = AssetImporter.GetAtPath(assetPath) as TextureImporter;
        if (texImporter != null)
        {
            texImporter.isReadable = isReadable;

            AssetDatabase.ImportAsset(assetPath);
            AssetDatabase.Refresh();
        }
    }
}