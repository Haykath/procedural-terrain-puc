﻿using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

using UnityEngine.Experimental.UIElements;
using UnityEngine.UI;

public class VertexData
{
    public Vector3 position;
    public Vector2 uv;
    public Color color;
    public Vector3 normal;
};

[CanEditMultipleObjects]
public class TerrainCreation : EditorWindow
{
    private static float noiseScale = 1.0f;
    private static int octavees = 8;

    // Base
    private int textureResolution = 1024;

    private Texture2D terrainTex;

    // Added
    private Material terrainMaterial;

    private bool offsetEdgeUvs = true;

    private TerrainGenerator.GenerationMode generationMode = TerrainGenerator.GenerationMode.Smooth;

    // Width and depth
    private int sideVertexCount = 50;

    private float sideScale = 100;

    // Height
    private float maxHeight = 25;

    private float minHeight = -25;

    private int smoothingIterations = 1;

    // Color
    private Color topColor = Color.red;

    private Color middleColor = Color.green;

    private Color bottomColor = Color.blue;

    private float topTextureThreshold = .8f;

    private float bottomTextureThreshold = .6f;

    private float blending = 1f;

    // Internal
    private GameObject terrainGameObject;

    private Color[] pix;

    private TerrainGenerator gen;

    private MeshFilter filter;

    private MeshRenderer renderer;

    //GUI
    private Vector2 scroll;

    private bool texFold = true;

    private bool terrainFold = true;

    private bool colorFold = true;

    public void OnGUI()
    {
        this.scroll = EditorGUILayout.BeginScrollView(this.scroll, false, true);
        EditorGUILayout.Separator();
        this.texFold = EditorGUILayout.Foldout(this.texFold, "Texture options");
        if (this.texFold)
        {
            noiseScale = EditorGUILayout.FloatField("Noise Scale", noiseScale);

            octavees = EditorGUILayout.IntSlider("Number of Octavees", octavees, 0, 8);

            this.textureResolution = EditorGUILayout.IntField("Texture Resolution", this.textureResolution);

            this.terrainTex = (Texture2D)EditorGUILayout.ObjectField(
                "Terrain texture",
                this.terrainTex,
                typeof(Texture2D),
                false);

            this.terrainMaterial = (Material)EditorGUILayout.ObjectField(
                "Terrain material",
                this.terrainMaterial,
                typeof(Material),
                false);

            this.offsetEdgeUvs = EditorGUILayout.Toggle("Offset Edge UVs", this.offsetEdgeUvs);
        }

        EditorGUILayout.Separator();
        this.terrainFold = EditorGUILayout.Foldout(this.terrainFold, "Terrain options");
        if (this.terrainFold)
        {

            this.generationMode =
                (TerrainGenerator.GenerationMode)EditorGUILayout.EnumPopup("Generation Mode", this.generationMode);

            this.smoothingIterations = EditorGUILayout.IntSlider(
                "Laplacian smooth iterations",
                this.smoothingIterations,
                0,
                5);

            this.sideVertexCount = EditorGUILayout.IntSlider("Terrain width and depth", this.sideVertexCount, 2, 100);

            this.sideScale = EditorGUILayout.FloatField("Scale in X and Z", this.sideScale);

            this.maxHeight = EditorGUILayout.FloatField("Maximum Height", this.maxHeight);

            this.minHeight = EditorGUILayout.FloatField("Minimum Height", this.minHeight);
        }

        EditorGUILayout.Separator();
        this.colorFold = EditorGUILayout.Foldout(this.colorFold, "Color options");
        if (this.colorFold)
        {

            this.topColor = EditorGUILayout.ColorField("Top color", this.topColor);

            this.middleColor = EditorGUILayout.ColorField("Middle color", this.middleColor);

            this.bottomColor = EditorGUILayout.ColorField("Bottom color", this.bottomColor);

            this.topTextureThreshold = EditorGUILayout.FloatField("Top Texture Threshold", this.topTextureThreshold);

            this.bottomTextureThreshold = EditorGUILayout.FloatField(
                "Bottom Texture Threshold",
                this.bottomTextureThreshold);

            this.blending = EditorGUILayout.FloatField("Blending radius", this.blending);
        }

        EditorGUILayout.Separator();

        if (GUILayout.Button("Create New Texture"))
        {
            this.CreateNoiseTexture();
        }

        if (GUILayout.Button("Generate Terrain"))
        {
            if (!this.terrainTex && !(this.terrainTex = AssetDatabase.LoadAssetAtPath<Texture2D>("Assets/TerrainTexture.png")))
            {
                this.CreateNoiseTexture();
            }

            this.CreateTerrainObject();
            this.GenerateTerrain();
        }

        EditorGUILayout.EndScrollView();
    }

    public float OctaveesNoise2D(float x, float y, int octNum, float frq, float amp)
    {
        float gain = 1.0f;
        float sum = 0.0f;

        for (int i = 0; i < octNum; i++)
        {
            sum += Mathf.PerlinNoise(x * gain / frq, y * gain / frq) * amp / gain;
            gain *= 2.0f;
        }

        return sum;
    }

    [MenuItem("Terrain/Create Terrain...")]
    private static void Init()
    {
        EditorWindow.GetWindow<TerrainCreation>().Show();
    }

    private void CreateNoiseTexture()
    {
        this.terrainTex = new Texture2D(this.textureResolution, this.textureResolution);
        this.pix = new Color[this.textureResolution * this.textureResolution];

        float xOri = UnityEngine.Random.value * 100000.0f;
        float yOri = UnityEngine.Random.value * 100000.0f;

        float y = 0.0f;
        while (y < this.terrainTex.height)
        {
            float x = 0.0f;
            while (x < this.terrainTex.width)
            {
                ////float xCoord = xOri + ((x / this.terrainTex.width) * noiseScale) + Mathf.Sin(y);
                ////float yCoord = yOri + ((y / this.terrainTex.height) * noiseScale);

                float sample = this.OctaveesNoise2D(xOri + (x / this.terrainTex.width), yOri + (y / this.terrainTex.height), octavees, 1.0f, 0.75f);

                this.pix[((int)y * this.terrainTex.width) + (int)x] = new Color(sample, sample, sample);

                x++;
            }

            y++;
        }

        this.terrainTex.SetPixels(pix);
        this.terrainTex.Apply();

        byte[] bytes = this.terrainTex.EncodeToPNG();

        Debug.Log("Creating Terrain Texture: " + Application.dataPath + "/TerrainTexture.png");

        File.WriteAllBytes(Application.dataPath + "/TerrainTexture.png", bytes);

        AssetDatabase.ImportAsset("Assets/TerrainTexture.png");
    }

    private void CreateTerrainObject()
    {
        if (!this.terrainGameObject && !(this.terrainGameObject = GameObject.FindGameObjectWithTag("Terrain")))
        {
            this.terrainGameObject = new GameObject("New Terrain");
            this.terrainGameObject.tag = "Terrain";
        }

        if (!this.filter && !(this.filter = this.terrainGameObject.GetComponent<MeshFilter>()))
        {
            this.filter = this.terrainGameObject.AddComponent<MeshFilter>();
        }

        if (!this.renderer && !(this.renderer = this.terrainGameObject.GetComponent<MeshRenderer>()))
        {
            this.renderer = this.terrainGameObject.AddComponent<MeshRenderer>();
        }
    }

    private void GenerateTerrain()
    {
        this.gen = new TerrainGenerator(this.terrainTex, this.sideVertexCount, this.sideVertexCount, this.generationMode)
        {
            WidthScale = this.sideScale,
            DepthScale = this.sideScale,
            IsAbsoluteHeight = false,
            OffsetEdgeUvs = this.offsetEdgeUvs,
            MaxHeight = this.maxHeight,
            MinHeight = this.minHeight,
            SmoothingIterations = (uint)this.smoothingIterations,
            BottomColor = this.bottomColor,
            MiddleColor = this.middleColor,
            TopColor = this.topColor,
            BottomTextureThreshold = this.bottomTextureThreshold,
            TopTextureThreshold = this.topTextureThreshold,
            Blending = this.blending
        };

        if (this.filter)
        {
            this.filter.sharedMesh = this.gen.GenerateMesh();
        }

        if (!this.terrainMaterial)
        {
            this.terrainMaterial = AssetDatabase.LoadAssetAtPath<Material>("Assets/Terrain.mat");
        }

        this.renderer.sharedMaterial = this.terrainMaterial;
    }
}